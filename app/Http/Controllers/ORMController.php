<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ORMModel;
use App\UniqueIdModel;

class ORMController extends Controller
{
    public $UniqueIdModel = "";

    public function __construct(){

        $this->UniqueIdModel = new UniqueIdModel();
    }
    //to delete the user from database but not in actual (just adding the deleted_at )
    public function delete()
    {
        $user = ORMModel::find(1);
        $user->delete();
    }
    //will disaplay only data where deleted at is null(ie. data not deleted )
    public function softdelete()
    {
        $users = ORMModel::all();
        echo '<pre>';
        print_r($users);

    }
    //will show all the data included deleted data 
    public function show_deleted_too()
    {
        $users = ORMModel::withTrashed()->where('status', 'active')->get();
        echo '<pre>';
        print_r($users);
    }
    //will display only deleted data in database
    public function show_deleted_only()
    {
        $users = ORMModel::onlyTrashed()->where('status', 'active')->get();
        echo '<pre>';
        print_r($users);
    }
    //it will again null the deleted_at value in database
    public function restore()
    {
        $users = ORMModel::where('status', 'active')->restore();
    
    }

    public function create(Request $request)
    {
       $user = $this->UniqueIdModel->add_user();
    }

    
}
